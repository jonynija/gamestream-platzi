//
//  Model.swift
//  GameStream
//
//  Created by Jonathan Ixcayau on 1/11/21.
//

import Foundation

struct GameResults: Codable, Hashable {
    let results: [Game]
}


// MARK: - Game
struct Game: Codable, Hashable {
    let title, studio, contentRating, publicationYear: String
    let gameDescription: String
    let platforms, tags: [String]
    let videosUrls: VideosUrls
    let galleryImages: [String]

    enum CodingKeys: String, CodingKey {
        case title, studio
        case contentRating = "contentRaiting"
        case publicationYear
        case gameDescription = "description"
        case platforms, tags, videosUrls, galleryImages
    }
}

// MARK: - VideosUrls
struct VideosUrls: Codable, Hashable {
    let mobile, tablet: String
}
