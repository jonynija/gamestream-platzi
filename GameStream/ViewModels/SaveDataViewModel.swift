//
//  SaveDataViewModel.swift
//  GameStream
//
//  Created by Jonathan Ixcayau on 7/06/22.
//

import Foundation

class SaveDataViewModel {
    
    var email: String = ""
    var password: String = ""
    var name: String = ""
    
    let userDataKey: String = "userData"
    
    func saveData(email: String, password: String, name: String) -> Bool {
        UserDefaults.standard.set([email, password, name],forKey: userDataKey)
        
        return true
    }
    
    func recoverData() -> [String] {
        let userData: [String] = UserDefaults.standard.stringArray(forKey: userDataKey)!
        
        return userData
    }
    
    func validate(email: String, password: String) -> Bool {
        var emailSaved = ""
        var passwordSaved = ""
        
        let savedData = UserDefaults.standard.stringArray(forKey: userDataKey)
        
        
        if(savedData != nil) {
            emailSaved = savedData![0]
            passwordSaved = savedData![1]
            
            if(email == emailSaved && password == passwordSaved){
                return true
            }
        }
        
        return false
    }    
}
