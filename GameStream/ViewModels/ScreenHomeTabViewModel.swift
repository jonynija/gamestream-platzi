//
//  ScreenHomeTabViewModel.swift
//  GameStream
//
//  Created by Jonathan Ixcayau on 1/11/21.
//

import Foundation

class ScreenHomeTabViewModel: ObservableObject {
    @Published var games = [Game]()
    
    func searchGame(field: String)  {
        games.removeAll()
        
        var newField = field
            .addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        newField = newField.trimmingCharacters(in: .whitespacesAndNewlines)
        
        let url = URL(string: "https://gamestream-api.herokuapp.com/api/games/search?contains=\(newField)")
        
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request) {data, response, error in
            do{
                if let jsonData = data {
                    let decodeData = try JSONDecoder().decode(GameResults.self, from: jsonData)
                    
                    DispatchQueue.main.async {
                        self.games.append(contentsOf: decodeData.results)
                    }
                }
            }catch{
                print("ERROR: \(error)")
            }
            
        }.resume()
    }
}
