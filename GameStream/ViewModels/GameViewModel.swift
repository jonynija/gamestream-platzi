//
//  ViewModel.swift
//  GameStream
//
//  Created by Jonathan Ixcayau on 1/11/21.
//

import Foundation
import UIKit

class GameViewModel: ObservableObject {
    
    @Published var games = [Game]()
    
    init(){
        let url = URL(string: "https://gamestream-api.herokuapp.com/api/games")
        
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request) {data, response, error in
            do{
                if let jsonData = data {
                    print("Length: \(jsonData)")
                    
                    
                    let decodeData = try JSONDecoder().decode([Game].self, from: jsonData)
                    
                    DispatchQueue.main.async {
                        self.games.append(contentsOf: decodeData)
                    }
                }
            }catch{
                print("ERROR: \(error)")
            }
            
        }.resume()
    }
}
