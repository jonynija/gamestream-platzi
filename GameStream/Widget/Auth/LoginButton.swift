//
//  LoginButton.swift
//  GameStream
//
//  Created by Jonathan Ixcayau on 15/09/21.
//

import SwiftUI

struct LoginButton: View {
    
    let text : String
    let action: () -> Void
    
    var body: some View {
        Button(action: action, label: {
            Text(text)
                .fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/)
                .foregroundColor(.white)
                .frame(maxWidth: .infinity, alignment: .center)
                .padding(
                    EdgeInsets(top: 11, leading: 18, bottom: 11, trailing: 18))
                .overlay(
                    RoundedRectangle(cornerRadius:6.0)
                        .stroke(Color("darkCianColor"), lineWidth: 1.0)
                        .shadow(color: .white, radius: 6)
                )
        })
        .padding(.bottom)
    }
}

struct LoginButton_Previews: PreviewProvider {
    static var previews: some View {
        LoginButton(
            text: "Login",
            action: {
                print("Printing values")
            }
        )
    }
}
