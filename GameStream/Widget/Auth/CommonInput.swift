//
//  CommonInput.swift
//  GameStream
//
//  Created by Jonathan Ixcayau on 15/09/21.
//

import SwiftUI
import Kingfisher

struct CommonInput: View {
    @Binding var content: String
    
    var title: String?
    var hint: String?
    var isPassword: Bool = false
    var hasBottomPadding: Bool = true
    
    @State var isActive = false
    
    var body: some View {
        VStack(alignment: .leading) {
            
            if title != nil && !title!.isEmpty{
                Text(title!)
                    .foregroundColor(Color("darkCianColor"))
            }
            
            ZStack(alignment: .leading){
                
                if content.isEmpty && hint != nil && !hint!.isEmpty{
                    Text(hint!)
                        .font(.caption)
                        .foregroundColor(.gray)
                }
                
                if isPassword{
                    SecureField(
                        "",
                        text: $content,
                        onCommit: {
                            isActive = false
                            
                        }
                    )
                    .foregroundColor(.white)
                    .onTapGesture {
                        isActive = true
                    }
                } else {
                    TextField(
                        "",
                        text: $content,
                        onEditingChanged: { (editingChanged) in
                            if editingChanged {
                                isActive = true
                            } else {
                                isActive = false
                            }
                        }
                    )
                    .foregroundColor(.white)
                }
            }
            
            Divider()
                .frame(height: 1)
                .background(isActive ?Color("darkCianColor") : Color.white)
        }
        .padding(.bottom, hasBottomPadding ? 8 : 0)
    }
}
