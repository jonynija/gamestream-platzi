//
//  SocialButton.swift
//  GameStream
//
//  Created by Jonathan Ixcayau on 15/09/21.
//

import SwiftUI

struct SocialButton: View {
    
    let title: String
    let action: () -> Void
    
    var body: some View {
        Button(
            action: action,
            label: {
                Text(title)
                    .fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/)
                    .foregroundColor(.white)
                    .padding(
                        EdgeInsets(top: 8, leading: 24,bottom: 8,trailing: 24))
                    .frame(maxWidth:.infinity)
                    .background(
                        RoundedRectangle(cornerRadius: 6)
                            .foregroundColor(Color("socialButtonColor"))
                    )
            }
        )
    }
}

struct SocialButton_Previews: PreviewProvider {
    static var previews: some View {
        SocialButton(
            title: "Facebook",
            action: {
                print("Facebook auth")
            }
        )
    }
}
