//
//  GameVideoPlayer.swift
//  GameStream
//
//  Created by Jonathan Ixcayau on 2/06/22.
//

import SwiftUI
import AVKit

struct GameVideoPlayer: View {
    let url: String
    
    var body: some View {
        VideoPlayer(
            player: AVPlayer(
                url: URL(string: url)!
            )
        )
        .ignoresSafeArea()
    }
}
