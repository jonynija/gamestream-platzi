//
//  GalleryImages.swift
//  GameStream
//
//  Created by Jonathan Ixcayau on 2/06/22.
//

import SwiftUI
import Kingfisher

@available(iOS 14.0, *)
struct GalleryImages: View {
    let images: [String]
    
    let gridWay = [
        GridItem(.flexible())
    ]
    
    var body: some View{
        VStack(alignment: .leading){
            Text("Galleria")
                .foregroundColor(.white)
                .font(.subheadline)
                .padding(.top,5)
                .padding(.leading)
            
            ScrollView(.horizontal){
                LazyHGrid(
                    rows: gridWay,
                    spacing: 8
                ){
                    ForEach(images, id: \.self){ url in
                        KFImage(URL(string: url))
                            .resizable()
                            .aspectRatio( contentMode: .fit )
                            .clipShape(RoundedRectangle.init(cornerRadius: 4))
                            .padding(.bottom, 12)
                    }
                }
            }
            .navigationBarHidden(true)
            .frame(height: 180)
        }
        .frame(
            maxWidth: .infinity,
            alignment: .leading
        )
    }
}
