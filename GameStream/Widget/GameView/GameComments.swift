//
//  GameComments.swift
//  GameStream
//
//  Created by Jonathan Ixcayau on 2/06/22.
//

import SwiftUI
import Kingfisher

struct GameComments: View {
    var body: some View {
        VStack(alignment:.leading){
            HomeBaseTitle(title: "Comentarios")
                .padding(.leading)
            
            GameSingleComment(
                photoUrl: "https://cdn.cloudflare.steamstatic.com/steam/apps/292030/ss_107600c1337accc09104f7a8aa7f275f23cad096.600x338.jpg",
                author: "Geoff Atto",
                daysAgo: "7",
                comment: "He visto que como media tiene una gran calificación, y estoy completamente de acuerdo. Es el mejor juego que he jugado sin ninguna duda, combina una buena trama con una buenísima experiencia de juego libre gracias a su inmenso mapa y actividades."
            )
            
            GameSingleComment(
                photoUrl: "https://cdn.cloudflare.steamstatic.com/steam/apps/292030/ss_ed23139c916fdb9f6dd23b2a6a01d0fbd2dd1a4f.600x338.jpg",
                author: "Alvy Baack",
                daysAgo: "12",
                comment: "He visto que como media tiene una gran calificación, y estoy completamente de acuerdo. Es el mejor juego que he jugado sin ninguna duda, combina una buena trama con una buenísima experiencia de juego libre gracias a su inmenso mapa y actividades. He visto que como media tiene una gran calificación, y estoy completamente de acuerdo. Es el mejor juego que he jugado sin ninguna duda, combina una buena trama con una buenísima experiencia de juego libre gracias a su inmenso mapa y actividades."
            )
        }
    }
}

struct GameSingleComment: View {
    let photoUrl: String
    let author: String
    let daysAgo: String
    let comment: String
    
    var body: some View {
        VStack(alignment: .leading) {
            HStack {
                KFImage(URL(string: photoUrl))
                    .resizable()
                    .aspectRatio( contentMode: .fill )
                    .clipShape(Circle())
                    .frame(width: 50, height: 50)
                
                VStack(alignment:.leading){
                    Text(author)
                        .fontWeight(.bold)
                        .foregroundColor(.white)
                        .font(.body)
                    
                    Text("Hace \(daysAgo) días")
                        .foregroundColor(.white)
                        .font(.body)
                }
                .padding(.leading, 8)
            }
            
            Text(comment)
                .foregroundColor(.white)
                .font(.body)
        }
        .frame(maxWidth: .infinity)
        .padding(.all)
        .background(Color("socialButtonColor"))
        .clipShape(RoundedRectangle.init(cornerRadius: 8))
        .padding(.all)
    }
}
