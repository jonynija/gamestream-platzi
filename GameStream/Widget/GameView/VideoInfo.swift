//
//  VideoInfo.swift
//  GameStream
//
//  Created by Jonathan Ixcayau on 2/06/22.
//

import SwiftUI

struct VideoInfo: View {
    let game: Game
    
    var body: some View {
        VStack(alignment:.leading){
            Text(game.title)
                .foregroundColor(.white)
                .font(.title)
                .padding(.leading)
            
            HStack{
                Text(game.studio)
                    .foregroundColor(.white)
                    .font(.subheadline)
                    .padding(.top,5)
                    .padding(.leading)
                
                Text(game.contentRating)
                    .foregroundColor(.white)
                    .font(.subheadline)
                    .padding(.top,5)
                    .padding(.leading)
                
                Text(game.publicationYear)
                    .foregroundColor(.white)
                    .font(.subheadline)
                    .padding(.top,5)
                    .padding(.leading)
            }
            
            
            Text(game.gameDescription)
                .foregroundColor(.white)
                .font(.subheadline)
                .padding(.top,5)
                .padding(.leading)
            
            HStack{
                ForEach(game.tags, id: \.self){ tag in
                    Text("#\(tag)")
                        .foregroundColor(.white)
                        .font(.subheadline)
                        .padding(.top,4)
                        .padding(.leading)
                    
                }
            }
        }
        .frame(maxWidth: .infinity, alignment: .leading)
    }
}
