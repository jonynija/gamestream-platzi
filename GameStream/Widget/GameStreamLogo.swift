//
//  GameStreamLogo.swift
//  GameStream
//
//  Created by Jonathan Ixcayau on 16/09/21.
//

import SwiftUI

struct GameStreamLogo: View {
    var body: some View {
        Image("logo")
            .resizable()
            .aspectRatio(contentMode: .fit)
            .frame(width: 250)
    }
}

struct GameStreamLogo_Previews: PreviewProvider {
    static var previews: some View {
        GameStreamLogo()
    }
}
