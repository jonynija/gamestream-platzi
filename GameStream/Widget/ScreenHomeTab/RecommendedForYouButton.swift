//
//  RecommendedForYouButton.swift
//  GameStream
//
//  Created by Jonathan Ixcayau on 1/11/21.
//

import SwiftUI

struct RecommendedForYouButton: View {
    let imagePath: String
    let action: () -> Void
    
    var body: some View {
        Button(
            action: action,
            label:{
                Image(imagePath)
                    .resizable()
                    .scaledToFit()
                    .frame(width: 240, height: 135)
            }
        )
    }
}

struct RecommendedForYouButton_Previews: PreviewProvider {
    static var previews: some View {
        RecommendedForYouButton(
            imagePath: "Abzu",
            action: {
                print("Button action")
            }
        )
    }
}
