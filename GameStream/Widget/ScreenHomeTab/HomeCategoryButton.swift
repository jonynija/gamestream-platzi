//
//  HomeCategoryButton.swift
//  GameStream
//
//  Created by Jonathan Ixcayau on 1/11/21.
//

import SwiftUI

struct HomeCategoryButton: View {

    let imagePath: String
    
    var body: some View{
        Button(
            action: {},
            label:{
                ZStack {
                    RoundedRectangle(cornerRadius: 12)
                        .fill(Color("socialButtonColor"))
                        .frame(width: 160, height: 100)
                    
                    VStack{
                        Image(imagePath)
                            .resizable()
                            .scaledToFit()
                            .frame(width: 42, height: 42)
                        
                        Text(imagePath.uppercased())
                            .font(.title3)
                            .foregroundColor(Color("darkCianColor"))
                    }
                }
            }
        )
    }
}

struct HomeCategoryButton_Previews: PreviewProvider {
    static var previews: some View {
        HomeCategoryButton(imagePath: "category")
    }
}
