//
//  ProfileSettings.swift
//  GameStream
//
//  Created by Jonathan Ixcayau on 7/06/22.
//

import SwiftUI

struct ProfileSettings: View {
    
    @State var isActive = true
    @State var isEditProvideViewActive = false
    
    var body: some View {
        VStack{
            NavigationLink(
                destination: EditProfileView(),
                isActive: $isEditProvideViewActive,
                label: {
                    EmptyView()
                }
            )
            
            Text("AJUSTES")
                .font(.title2)
                .fontWeight(.bold)
                .foregroundColor(.white)
            
            Button(
                action: {
                    isEditProvideViewActive.toggle()
                },
                label: {
                    HStack {
                        Text("Editar perfil")
                            .foregroundColor(Color.white)
                        
                        Spacer()
                        
                        Text(">")
                            .foregroundColor(Color.white)
                    }
                }
            )
            .padding()
            .background(Color("socialButtonColor"))
            .clipShape(
                RoundedRectangle(cornerRadius: 1)
            )
            .padding(.vertical)
            
            Button(
                action: {
                    
                },
                label: {
                    HStack {
                        Text("Notificaciones")
                            .foregroundColor(Color.white)
                        
                        
                        Spacer()
                        
                        Toggle(
                            "",
                            isOn: $isActive
                        )
                    }
                }
            )
            .padding()
            .background(Color("socialButtonColor"))
            .clipShape(
                RoundedRectangle(cornerRadius: 1)
            )
        }
    }
}

struct ProfileSettings_Previews: PreviewProvider {
    static var previews: some View {
        ProfileSettings()
    }
}
