//
//  GameView.swift
//  GameStream
//
//  Created by Jonathan Ixcayau on 1/11/21.
//

import SwiftUI

struct GameView: View {
    var game: Game
    
    var body: some View {
        ZStack{
            Color("backgroundColor")
                .ignoresSafeArea()
            
            VStack{
                GameVideoPlayer(url: game.videosUrls.mobile)
                    .frame(height: 200)
                
                ScrollView{
                    VideoInfo(game: game)
                    
                    GalleryImages(images: game.galleryImages)
                    
                    GameComments()
                }
                .frame(maxWidth: .infinity)
            }
        }
    }
}

struct GameView_Previews: PreviewProvider {
    static var previews: some View {
        GameView(game:
                    Game(title: "The Witcher 3: Wild Hunt",
                         studio: "CD Project Red",
                         contentRating: "M",
                         publicationYear: "2015",
                         gameDescription:"As war rages on throu",
                         platforms: ["Pc","Play"],
                         tags:["RPG","Action"],
                         videosUrls:VideosUrls(
                            mobile: "https://dl.dropboxusercontent.com/s/k6g0zwmsxt9qery/TheWitcher480.mp4",
                            tablet: "https://dl.dropboxusercontent.com/s/9faic5dtebavp1o/TheWitcherMax.mp4"),
                         galleryImages: [
                            "https://cdn.cloudflare.steamstatic.com/steam/apps/292030/ss_107600c1337accc09104f7a8aa7f275f23cad096.600x338.jpg",
                            "https://cdn.cloudflare.steamstatic.com/steam/apps/292030/ss_ed23139c916fdb9f6dd23b2a6a01d0fbd2dd1a4f.600x338.jpg",
                            "https://cdn.cloudflare.steamstatic.com/steam/apps/292030/ss_908485cbb1401b1ebf42e3d21a860ddc53517b08.600x338.jpg"
                         ]
                        )
        )
    }
}

