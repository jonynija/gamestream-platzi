//
//  EditProfileView.swift
//  GameStream
//
//  Created by Jonathan Ixcayau on 7/06/22.
//

import SwiftUI

struct EditProfileView: View {
    
    @State var imageProfile: Image? = Image("profilePicture")
    @State var pickerShowed: Bool = false
    
    var body: some View {
        ZStack{
            Color("backgroundColor")
                .ignoresSafeArea()
            
            ScrollView{
                
                VStack {
                    Text("Editar Perfil")
                        .font(.title2)
                        .fontWeight(.bold)
                        .foregroundColor(.white)
                        .frame(minWidth: 0, idealWidth: 100, maxWidth: .infinity, alignment: .center)
                        .padding(.bottom, 16)
                    
                    Button(
                        action: takePhoto,
                        label: {
                            ZStack{
                                imageProfile!
                                    .resizable()
                                    .aspectRatio(contentMode: .fit)
                                    .frame(width: 80, height: 80)
                                    .clipShape(Circle())
                                
                                Image(systemName: "camera")
                                    .foregroundColor(.white)
                            }
                        }
                    )
                    .padding(.bottom, 16)
                    .sheet(
                        isPresented: $pickerShowed,
                        content: {
                            SUImagePickerView(sourceType: .photoLibrary, image: $imageProfile, isPresented: $pickerShowed)
                        }
                    )
                    
                    EditModule()
                    
                }
            }
            .padding(.horizontal, 6)
        }
    }
    
    func takePhoto(){
        pickerShowed = true
    }
}

struct EditModule: View {
    
    @State var email: String = ""
    @State var password: String = ""
    @State var name: String = ""
    
    var body: some View {
        VStack(alignment: .leading){
            CommonInput(
                content: $email,
                title: "Correo",
                hint: "ejemplo@correo.com"
            )
            
            CommonInput(
                content: $password,
                title: "Contraseña",
                hint: "**********",
                isPassword: true
            )
            
            CommonInput(
                content: $name,
                title: "Nombre",
                hint: "Introduce tu nombre de usuario"
            )
            
            LoginButton(
                text: "Actualizar Datos",
                action: {
                    updateData()
                }
            )
        }
    }
    
    
    func updateData(){
        let saveDataViewModel = SaveDataViewModel()
        
        let result = saveDataViewModel.saveData(email: email, password: password, name: name)
        
        print("Saved data \(result)")
    }
}

struct EditProfileView_Previews: PreviewProvider {
    static var previews: some View {
        EditProfileView()
    }
}
