//
//  HomeView.swift
//  GameStream
//
//  Created by Jonathan Ixcayau on 16/09/21.
//

import SwiftUI

struct HomeView: View {
    
    @State var tabHome: Int = 2
    
    var body: some View {
        TabView(selection:$tabHome){
            ProfileTab()
                .tabItem {
                    Image(systemName: "person")
                    Text("Perfil")
                }
                .tag(0)
            
            GamesViewTab()
                .tabItem {
                    Image(systemName: "gamecontroller")
                    Text("Juegos")
                }
                .tag(1)
            
            ScreenHomeTab()
                .tabItem {
                    Image(systemName: "house")
                    Text("Inicio")
                }
                .tag(2)
            
            FavoritesTab()
                .tabItem {
                    Image(systemName: "heart")
                    Text("Favoritos")
                }
                .tag(3)
        }
        .accentColor(.white)
        .navigationBarHidden(true)
        .navigationBarBackButtonHidden(true)
    }
    
    init() {
        UITabBar.appearance().barTintColor = UIColor(Color("tabBarColor"))
        UITabBar.appearance().backgroundColor = UIColor(Color("tabBarColor"))
        UITabBar.appearance().isTranslucent = true
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
 
