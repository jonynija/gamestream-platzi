//
//  GamesViewTab.swift
//  GameStream
//
//  Created by Jonathan Ixcayau on 1/11/21.
//

import SwiftUI
import Kingfisher

struct GamesViewTab: View {
    @ObservedObject var model = GameViewModel()
    
    @State var gameViewIsActive: Bool = false
    @State var game: Game?
    
    let gridWay = [
        GridItem(.flexible()),
        GridItem(.flexible())
    ]
    
    var body: some View {
        ZStack{
            Color("backgroundColor")
                .ignoresSafeArea()
            
            VStack{
                Text("Juegos")
                    .font(.title2)
                    .fontWeight(.bold)
                    .foregroundColor(.white)
                    .padding(EdgeInsets(top: 16, leading: 0, bottom: 64, trailing: 0))
                
                ScrollView{
                    LazyVGrid(
                        columns: gridWay,
                        spacing: 8
                    ){
                        ForEach(model.games, id: \.self){ game in
                            
                            Button(
                                action: {
                                    self.game = game
                                    gameViewIsActive = true
                                },
                                label: {
                                    KFImage(URL(string: game.galleryImages[0]))
                                        .resizable()
                                        .aspectRatio( contentMode: .fit )
                                        .clipShape(RoundedRectangle.init(cornerRadius: 4))
                                        .padding(.bottom, 12)
                                    
                                }
                            )
                        }
                    }
                }
            }
            .padding(.horizontal, 6)
            
            if let game = game {
                NavigationLink(
                    destination: GameView(game: game),
                    isActive: $gameViewIsActive,
                    label:{
                        EmptyView()
                    }
                )
            }
        }
        .navigationBarHidden(true)
        .navigationBarBackButtonHidden(true)
    }
}

struct GamesViewTab_Previews: PreviewProvider {
    static var previews: some View {
        GamesViewTab()
    }
}
