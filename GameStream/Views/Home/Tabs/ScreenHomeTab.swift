//
//  ScreenHomeTab.swift
//  GameStream
//
//  Created by Jonathan Ixcayau on 16/09/21.
//

import SwiftUI
import AVKit

struct ScreenHomeTab: View {
    
    @ObservedObject var model:ScreenHomeTabViewModel = ScreenHomeTabViewModel()
    
    @State var searchText: String = ""
    @State var isPlayerActive = false
    @State var url : String = "https://cdn.cloudflare.steamstatic.com/steam/apps/256658589/movie480.mp4"
    @State var isGameInfoEmpty = false
    @State var isSearchTextEmpty = false
    
    @State var isGameViewActive = false
    @State var currentGame: Game?
    
    let  urlVideos: [String] = [
        "https://cdn.cloudflare.steamstatic.com/steam/apps/256658589/movie480.mp4",
        "https://cdn.cloudflare.steamstatic.com/steam/apps/256671638/movie480.mp4",
        "https://cdn.cloudflare.steamstatic.com/steam/apps/256720061/movie480.mp4",
        "https://cdn.cloudflare.steamstatic.com/steam/apps/256814567/movie480.mp4",
        "https://cdn.cloudflare.steamstatic.com/steam/apps/256705156/movie480.mp4",
        "https://cdn.cloudflare.steamstatic.com/steam/apps/256801252/movie480.mp4",
        "https://cdn.cloudflare.steamstatic.com/steam/apps/256757119/movie480.mp4"
    ]
    
    
    var body: some View {
        ZStack{
            Color("backgroundColor")
                .ignoresSafeArea()
            
            ScrollView{
                VStack {
                    GameStreamLogo()
                        .padding(.bottom)
                    
                    HStack{
                        Button(
                            action: search,
                            label: {
                                Image(systemName: "magnifyingglass")
                                    .foregroundColor(
                                        searchText.isEmpty ? Color(.yellow) : Color("darkCianColor"))
                            }
                        )
                        .alert(isPresented: $isGameInfoEmpty){
                            Alert(
                                title: Text("Error"),
                                message: Text("No se encontro el juego"),
                                dismissButton: .default(Text("Entendido"))
                            )
                        }
                        .alert(isPresented: $isSearchTextEmpty){
                            Alert(
                                title: Text("Error"),
                                message: Text("Debes ingresar texto para realizar la busqueda"),
                                dismissButton: .default(Text("Entendido"))
                            )
                        }
                        
                        CommonInput(content: $searchText, hint: "Buscar un video", hasBottomPadding: false)
                    }
                    .padding([.top, .bottom], 11)
                    .padding([.leading, .trailing], 18)
                    .background(Color("tabBarColor"))
                    .clipShape(Capsule())
                    
                    PopularsView(urlPlayer: $url, isPlayerActive: $isPlayerActive, url: urlVideos[0] )
                    SuggestedCategories()
                    RecommendedForYou(urlPlayer: $url, isPlayerActive: $isPlayerActive, urlVideos: urlVideos)
                    
                    
                    NavigationLink(
                        destination: VideoPlayer(player: AVPlayer(url: URL(string: url)!))
                            .frame(width: 400, height: 300),
                        isActive: $isPlayerActive,
                        label: {
                            EmptyView()
                        }
                    )
                    
                    if let game = currentGame {
                        NavigationLink(
                            destination: GameView(game: game),
                            isActive: $isGameViewActive,
                            label: {
                                EmptyView()
                            }
                        )
                    }
                }
            }
            .padding(.horizontal, 24)
        }
        .navigationBarHidden(true)
        .navigationBarBackButtonHidden(true)
    }
    
    func search() {
        isSearchTextEmpty = searchText.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty
        if isSearchTextEmpty {
            return;
        }
        
        model.searchGame(field: searchText)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.3) {
            print("Length: \(model.games.count)")
            
            if model.games.count == 0 {
                isGameInfoEmpty = true
            }else{
                currentGame = model.games[0]
                isGameViewActive = true
            }
        }
    }
}

struct PopularsView: View {
    @Binding var urlPlayer: String
    @Binding var isPlayerActive: Bool
    
    let url: String
    
    var body: some View {
        VStack  {
            HomeBaseTitle(title: "LOS MÁS POPULARES")
            
            ZStack{
                Button(
                    action: {
                        urlPlayer = url
                        isPlayerActive = true
                    },
                    label: {
                        VStack(spacing:0){
                            ZStack{
                                Image("thewitcher")
                                    .resizable()
                                    .scaledToFit()
                                    .frame(minWidth: 0,  maxWidth: .infinity, minHeight: 200, maxHeight: 200)
                                
                                Image(systemName: "play.circle.fill")
                                    .resizable()
                                    .foregroundColor(.white)
                                    .frame(width: 42, height: 42)
                            }
                            .padding(.bottom, 4)
                            
                            Text("The Witcher 3")
                                .frame(minWidth: 0, maxWidth: .infinity, alignment: .leading)
                        }
                    }
                )
                .frame(minWidth: 0, maxWidth: .infinity, alignment: .center)
                .background(Color("tabBarColor"))
            }
        }
        .navigationBarHidden(true)
        .navigationBarBackButtonHidden(true)
    }
}

struct SuggestedCategories:View {
    
    var body: some View {
        VStack{
            HomeBaseTitle(title:"CATEGORÍAS SUGERIDAS PARA TI")
            
            ScrollView(.horizontal, showsIndicators: false){
                HStack{
                    HomeCategoryButton(imagePath: "category")
                    HomeCategoryButton(imagePath: "fps")
                    HomeCategoryButton(imagePath: "rpg")
                    HomeCategoryButton(imagePath: "world")
                }
            }
        }
    }
}

struct RecommendedForYou:View {
    @Binding var urlPlayer: String
    @Binding var isPlayerActive: Bool
    
    let urlVideos: [String]
    
    var body: some View {
        VStack{
            HomeBaseTitle(title:"RECOMENDADO PARA TI")
            
            ScrollView(.horizontal, showsIndicators: false){
                HStack{
                    RecommendedForYouButton(
                        imagePath: "Abzu",
                        action: {
                            urlPlayer = urlVideos[1]
                            isPlayerActive = true
                        }
                    )
                    RecommendedForYouButton(
                        imagePath: "Crash Bandicoot",
                        action: {
                            urlPlayer = urlVideos[2]
                            isPlayerActive = true
                        }
                    )
                    RecommendedForYouButton(
                        imagePath: "DEATH STRANDING",
                        action: {
                            urlPlayer = urlVideos[3]
                            isPlayerActive = true
                        }
                    )
                }
            }
        }
        .padding(.bottom, 20)
    }
}

struct HomeBaseTitle:View {
    let title : String
    
    var body: some View {
        Text(title)
            .font(.title3)
            .foregroundColor(.white)
            .bold()
            .frame(minWidth: 0, maxWidth: .infinity, alignment: .leading)
            .padding(.top)
    }
}

struct ScreenHomeTab_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
