//
//  ProfileTab.swift
//  GameStream
//
//  Created by Jonathan Ixcayau on 7/06/22.
//

import SwiftUI

struct ProfileTab: View {
    
    @State var userName = "User name"
    
    var body: some View {
        ZStack{
            Color("backgroundColor")
                .ignoresSafeArea()
            
            VStack{
                Text("Perfil")
                    .font(.title2)
                    .fontWeight(.bold)
                    .foregroundColor(.white)
                    .frame(minWidth: 0, idealWidth: 100, maxWidth: .infinity, alignment: .center)
                
                
                VStack{
                    Image("profilePicture")
                        .resizable()
                        .aspectRatio(contentMode: .fill)
                        .frame(width: 118, height: 118)
                        .clipShape(Circle())
                    
                }
                .padding(EdgeInsets(top: 16, leading: 0, bottom: 32, trailing: 0))
                
                ProfileSettings()
                
            }
            .padding(.horizontal, 6)
        }
        .navigationBarHidden(true)
        .navigationBarBackButtonHidden(true)
        .onAppear(
            perform: {
                print("Checking existing user data in UserDefaults")
            }
        )
    }
}

struct ProfileTab_Previews: PreviewProvider {
    static var previews: some View {
        ProfileTab()
    }
}
