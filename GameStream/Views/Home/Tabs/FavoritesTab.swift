//
//  FavoritesTab.swift
//  GameStream
//
//  Created by Jonathan Ixcayau on 3/06/22.
//

import SwiftUI
import AVKit

struct FavoritesTab: View {
    @ObservedObject var allGames = GameViewModel()
    
    var body: some View {
        ZStack{
            Color("backgroundColor")
                .ignoresSafeArea()
            
            VStack{
                Text("Favoritos")
                    .font(.title2)
                    .fontWeight(.bold)
                    .foregroundColor(.white)
                    .padding(EdgeInsets(top: 16, leading: 0, bottom: 64, trailing: 0))
                
                ScrollView{
                    ForEach(allGames.games, id: \.self){ game in
                        VStack(spacing:0){
                            VideoPlayer(
                                player: AVPlayer(url:URL(string: game.videosUrls.mobile)!)
                            )
                            .frame(height: 300)
                            
                            Text(game.title)
                                .foregroundColor(Color.white)
                                .padding()
                                .frame(maxWidth: .infinity, alignment: .leading)
                                .background(Color("tabBarColor"))
                        }
                        .clipShape(RoundedRectangle(cornerRadius: 6.0))
                        .padding(.bottom, 10)
                    }
                }
                .padding(.horizontal, 8)
            }
            .padding(.horizontal, 6)
        }
        .navigationBarHidden(true)
        .navigationBarBackButtonHidden(true)
    }
}

struct FavoritesTab_Previews: PreviewProvider {
    static var previews: some View {
        FavoritesTab()
    }
}
