//
//  LoginView.swift
//  GameStream
//
//  Created by Jonathan Ixcayau on 14/09/21.
//

import SwiftUI

struct LoginView: View {
    @State var emailText: String = ""
    @State var passwordText: String = ""
    @State var isHomeActive: Bool = false
    
    var body: some View {
        VStack {
            Spacer()
            
            CommonInput(content: $emailText, title: "Correo", hint: "ejemplo@correo.com")
            
            CommonInput(content: $passwordText, title: "Contraseña", hint: "contraseña", isPassword: true)
            
            Text("Olvidaste tu contraseña?")
                .foregroundColor(Color("darkCianColor"))
                .font(.footnote)
                .frame(maxWidth: .infinity, alignment: .trailing)
                .padding(.bottom)
            
            LoginButton(text: "Iniciar sesión", action: login)
            
            Spacer()
            
            LoginButtonsView()
            
            Spacer()
            
            NavigationLink(
                destination: HomeView(),
                isActive: $isHomeActive,
                label: {
                    EmptyView()
                })
        }
    }
    
    
    func login() {
        print(emailText , passwordText)
        
        isHomeActive = true
    }
}

struct LoginButtonsView: View {
    var body: some View{
        VStack{
            Text("Iniciar sesión con redes sociales")
                .foregroundColor(.white)
                .font(.body)
                .frame(maxWidth: .infinity, alignment: .center)
                .padding(.bottom)
            
            HStack{
                SocialButton(title: "Facebook", action: facabookLogin)
                
                SocialButton(title: "Twitter", action: twitterLogin)
            }
        }
    }
    
    func facabookLogin() {
        print("Login with Facebook")
    }
    
    func twitterLogin() {
        print("Login with Twitter")
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        AuthView()
    }
}
