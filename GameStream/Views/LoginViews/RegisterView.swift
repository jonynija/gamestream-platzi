//
//  RegisterView.swift
//  GameStream
//
//  Created by Jonathan Ixcayau on 15/09/21.
//

import SwiftUI

struct RegisterView: View {
    @State var emailText: String = ""
    @State var passwordText: String = ""
    @State var confirmPaswordText: String = ""
    
    var body: some View {
        VStack {
            Spacer()
            
            VStack(alignment: .center){
                Text("Elije una foto de perfil")
                    .fontWeight(.bold)
                    .foregroundColor(.white)
                
                Text("Puedes cambiar o elegirla m")
                    .fontWeight(.bold)
                    .foregroundColor(.white)
                    .padding(.bottom)
                
                Button(action: takePhoto, label: {
                    ZStack{
                        Image("profilePicture")
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(width: 80, height: 80)
                        
                        Image(systemName: "camera")
                            .foregroundColor(.white)
                    }
                    
                })
            }
            .padding(.bottom, 30)
            
            CommonInput(content: $emailText, title: "Correo", hint: "ejemplo@correo.com")
            
            CommonInput(content: $passwordText, title: "Contraseña", hint: "contraseña", isPassword: true)
            
            CommonInput(content: $confirmPaswordText, title: "Confirmar contraseña", hint: "password", isPassword: true)
                .padding(.bottom, 20)
            
            LoginButton(text: "Registrarse") {
                register(email: emailText, password: passwordText, confirmPassword: confirmPaswordText)
            }
            
            Spacer()
            
            RegisterButtonsView()
            
            Spacer()
        }
    }
}

struct RegisterButtonsView: View {
    var body: some View{
        VStack{
            Text("Registrarse con redes sociales")
                .foregroundColor(.white)
                .font(.body)
                .frame(maxWidth: .infinity, alignment: .center)
                .padding(.bottom)
            
            HStack{
                SocialButton(title: "Facebook", action: facebookRegister)
                
                SocialButton(title: "Twitter", action: twitterRegister)
            }
        }
    }
}

func takePhoto() {
    print("Take photo")
}

func register(email: String, password: String, confirmPassword: String) {
    print(email, password, confirmPassword)
}

func facebookRegister() {
    print("Register with Facebook")
}

func twitterRegister() {
    print("Register with Twitter")
}

struct RegisterView_Previews: PreviewProvider {
    static var previews: some View {
        AuthView()
    }
}
