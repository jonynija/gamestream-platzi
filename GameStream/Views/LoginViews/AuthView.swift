//
//  LoginView.swift
//  GameStream
//
//  Created by Jonathan Ixcayau on 14/09/21.
//

import SwiftUI

struct AuthView: View {
    var body: some View {
        NavigationView{
            ZStack {
                Color("backgroundColor")
                    .ignoresSafeArea()
                
                GeometryReader { proxy in
                    
                    ScrollView{
                        
                        VStack{
                            
                            Spacer()
                            
                            GameStreamLogo()
                                .padding(.bottom, 40)
                            
                            LoginAndRegisterView()
                        }
                        .frame(minHeight: proxy.size.height)
                    }
                }
            }
            .navigationBarHidden(true)
        }
        .navigationViewStyle(StackNavigationViewStyle())
    }
}

struct LoginAndRegisterView :View {
    
    @State var isLogin : Bool = true
    
    var body : some View{
        VStack{
            HStack{
                Spacer()
                
                Button("INICIA SESIÓN") {
                    isLogin = true
                }
                .foregroundColor(isLogin ? .white:  .gray)
                
                Spacer()
                
                Button("REGÍSTRATE") {
                    isLogin = false
                }
                .foregroundColor(!isLogin ? .white : .gray)
                
                Spacer()
            }
            .padding(.bottom, 20)
            
            if isLogin {
                LoginView()
            }else{
                RegisterView()
            }
        }
        .padding(.horizontal, 24)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        AuthView()
            .previewDevice("iPad Pro (9.7-inch)")
            .previewInterfaceOrientation(.portraitUpsideDown)
    }
}

