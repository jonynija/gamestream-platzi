//
//  GameStreamApp.swift
//  GameStream
//
//  Created by Jonathan Ixcayau on 14/09/21.
//

import SwiftUI

@main
struct GameStreamApp: App {
    var body: some Scene {
        WindowGroup {
            AuthView()
        }
    }
}
